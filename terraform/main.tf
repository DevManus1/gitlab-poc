# Creating VPC
resource "aws_vpc" "cicd_vpc" {
  cidr_block       = "${var.vpc_cidr}"  # The CIDR block for the VPC
  instance_tenancy = "default"          # The tenancy of the VPC

  tags = {
    Name = "cicd_vpc"                   # A tag for the VPC
  }
}

# Creating Subnet
resource "aws_subnet" "cicd_vpc_subnet" {
  vpc_id     = aws_vpc.cicd_vpc.id          # The ID of the VPC the subnet should be created in
  availability_zone = "us-east-1a"          # The availability zone for the subnet
  cidr_block = "${var.subnet_cidr}"         # The CIDR block for the subnet

  tags = {
    Name = "cicd_vpc_subnet"                # A tag for the subnet
  }
}

# Create an internet gateway and attach it to the VPC
resource "aws_internet_gateway" "cicd_internet_gateway" {
  vpc_id = aws_vpc.cicd_vpc.id              # The ID of the VPC to attach the internet gateway to

  tags = {
    Name = "cicd_internet_gateway"          # A tag for the internet gateway
  }
}

# Create a route table with a default route to the internet gateway and associate it with the public subnet
resource "aws_route_table" "cicd_vpc_route_table" {
  vpc_id = aws_vpc.cicd_vpc.id          # The ID of the VPC to associate the route table with

  route {
    cidr_block = "0.0.0.0/0"            # The destination CIDR block for the default route
    gateway_id = aws_internet_gateway.cicd_internet_gateway.id  # The ID of the internet gateway to use for the default route
  }

  tags = {
    Name = "cicd_public_route_table"   # A tag for the route table
  }
}

# Associate the route table with the subnet
resource "aws_route_table_association" "cicd_vpc_subnet_association" {
  subnet_id      = aws_subnet.cicd_vpc_subnet.id        # The ID of the subnet to associate the route table with
  route_table_id = aws_route_table.cicd_vpc_route_table.id  # The ID of the route table to associate with the subnet
}

# Create a security group for the Gitlab instance
resource "aws_security_group" "gitlab_sg" {
  name_prefix = "gitlab_sg"
  vpc_id      = aws_vpc.cicd_vpc.id

# Enable SSH Access
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Enable HTTP access 
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Enable HTTPS access
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Allow outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "gitlab_sg"
  }
}

# Create a security group for the Gitlab runners
resource "aws_security_group" "gitlab_runner_sg" {
  name_prefix = "gitlab_runner_sg"
  vpc_id      = aws_vpc.cicd_vpc.id

# Enable SSH Access
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Enable HTTP access 
   ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Enable HTTPS access
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

# Allow outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"              #"-1" indicates any protocol is allowed. 
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "gitlab_runner_sg"
  }
}

# Launch the Gitlab instance using a Ubuntu 22.04 
resource "aws_instance" "gitlab_instance" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.gitlab_sg.id]
  subnet_id              = aws_subnet.cicd_vpc_subnet.id
  associate_public_ip_address = true
  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
              curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
              sudo EXTERNAL_URL=${var.gitlab_hostname} apt-get install gitlab-ee
              sleep 300
              sudo cat /etc/gitlab/initial_root_password | grep "Password:"
              EOF

  tags = {
    Name = "gitlab_instance"
  }
}

resource "null_resource" "gitlab_password" {
  depends_on = [ aws_instance.gitlab_instance ]
  connection {
    type        = "ssh"
    user        = var.instance_username
    private_key = var.aws_ssh_private_key
    host        = aws_instance.gitlab_instance.public_ip
  }

  provisioner "remote-exec" {
    inline = [  
    "echo 'Waiting for user data script to finish'",  
    "cloud-init status --wait > /dev/null",
    "sudo cat /etc/gitlab/initial_root_password | grep 'Password:'"
    ] 
  }
}

# Add the Instance IP as the A record for the domain.
resource "aws_route53_record" "gitlab_record" {
  depends_on = [aws_instance.gitlab_instance]
  zone_id = var.route53_zone_id
  name    = var.gitlab_hostname
  type    = "A"
  ttl     = "300"
  records = [aws_instance.gitlab_instance.public_ip]
}


# Launch the Gitlab runner using a official package
# Install Docker on the runner host 
resource "aws_instance" "gitlab_runner1_instance" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.gitlab_runner_sg.id]
  subnet_id              = aws_subnet.cicd_vpc_subnet.id
  associate_public_ip_address = true
  user_data = <<-EOF
              #!/bin/bash
              sudo snap install docker
              sudo systemctl status docker
              curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
              sudo apt install -y gitlab-runner
              EOF

  tags = {
    Name = "gitlab_runner1_instance"
  }
}



#Register the runner
resource "null_resource" "register_gitlab_runner1" {
  depends_on = [aws_instance.gitlab_runner1_instance]

  connection {
    type        = "ssh"
    user        = var.instance_username
    private_key = var.aws_ssh_private_key
    host        = aws_instance.gitlab_runner1_instance.public_ip
  }


  provisioner "file" {
    source      = "scripts/register_runner1.sh"
    destination = "/tmp/register.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo echo ${var.gitlab_registration_token}",
      "echo 'url: http://${var.gitlab_hostname}'",
      "echo 'Setting file permission'",
      "chmod 755 /tmp/register.sh",
      "sudo ls -l /tmp/register.sh",
      "/bin/bash /tmp/register.sh ${var.gitlab_registration_token} ${var.gitlab_hostname}",
      "sudo systemctl enable gitlab-runner.service",
      "sudo systemctl start gitlab-runner.service"
    ]
  }
}