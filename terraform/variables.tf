variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "instance_type" {
  type    = string
  default = "c5.xlarge"
}

variable "ami_id" {
  description = "The ID of the AMI to use for the instances"
  default = "ami-007855ac798b5175e"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "subnet_cidr" {
  type    = string
  default = "10.0.1.0/24"
}

variable "key_name" {
  description = "The SSH key pair to use for instance access"
  default = "GitLab"
}

variable "gitlab_hostname" {
  type = string
  default = "pakyabyte.com"
}

variable "route53_zone_id" {
    type = string
    default = "Z018888923NA3NAIQSMJ5"
}

variable "instance_username" {
  type = string
  default = "ubuntu"
}

variable "gitlab_registration_token" {
  description = "Token"
  type        = string
  default     = ""
}

variable "aws_ssh_private_key" {
  description = "SSH private key for accessing EC2 instances"
  type        = string
  default     = ""
}