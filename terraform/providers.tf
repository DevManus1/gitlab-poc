# Specify the Terraform version and required providers
terraform {
  # Define the required provider and version constraints
  required_providers {
    aws = {
      source  = "hashicorp/aws"   # The source of the AWS provider
      version = "~> 3.27"         # The version constraints for the AWS provider
    }
  }
  
  # Specify the backend configuration for storing Terraform state remotely
  backend "http" {} 
}

# Configure the AWS provider
provider "aws" {
  # Use the AWS region specified by the aws_region variable
  region     = "${var.aws_region}"
}
