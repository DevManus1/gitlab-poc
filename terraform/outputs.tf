output "gitlab_instance_public_ip" {
  value = aws_instance.gitlab_instance.public_ip
}

output "gitlab_runner1_instance_public_ip" {
  value = aws_instance.gitlab_runner1_instance.public_ip
}