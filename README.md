# GitLab Takehome Assessment

This project provides a simple way to use GitLab to run a pipeline that leverages Terraform and GitLab-managed Terraform Http backend to provision a self-managed Gitlab Omnibus instance and Runner on a Ubuntu 22.04 LTS instance running on AWS. 

## Instructions to run the pipeline

You can go to pipelines and re-run the latest pipeline to re-provision the infrastructure. Once done, manually run the destroy stage to un-provision.

Alternatively, a commit to the main branch should trigger the pipeline too. 

## Getting Started

The below steps highlight the implementation plan for this project.

## Pre-requisites 

Before you begin to provision infrastructure on AWS, you will need an AWS account. You can sign up for the free tier to explore and play around by going to this [link](aws.amazon.com/free). 

Once you have an account, you will need to set up the following items:
1. Create an IAM User.
2. Generate AWS Access Keys for the above user.
3. Generate SSH key-pair for EC2 Instances.

Secondly, you will need access to GitLab and access to a repository. You can sign up for a free account at [Gitlab](www.gitlab.com). You will also need a PAT(Project Access Token) that will be used to access the repository. 

Lastly, you will need a public domain or a hosted zone to access the instance via a URL. This step is optional, as Gitlab will autodetect the AWS EC2 Instance's public IP as the hostname, and you should be able to log in via the IP. 

## Terraform Backend
In this example, I will be using the Gitlab-managed terraform HTTP backend. This is required to store the state of your infrastructure to prevent conflicting changes to the infrastructure. 

In Gitlab CI, this file is stored in Project > Infrastructure > Terraform > <State_File>  

## Deploying Gitlab Environment in AWS
This repository contains Terraform code for deploying a Gitlab environment in AWS. The infrastructure includes a Gitlab instance and a Gitlab runner instance deployed in a VPC with a public subnet. The following resources will be created:

### VPC
A VPC (Virtual Private Cloud) is a logically isolated network within the AWS cloud. It enables users to launch resources in a virtual network that they define. In this project, a VPC is created with a specified CIDR block, which defines the range of IP addresses in the VPC. The VPC is the base infrastructure component for the Gitlab environment and enables other components such as subnets, route tables, and security groups to be associated with it.

### Subnet
A subnet is a range of IP addresses within a VPC. It allows users to group resources based on their function and allocate resources to different availability zones. In this project, a subnet is created within the VPC in a specified availability zone. The CIDR block for the subnet is specified separately from the VPC CIDR block. This subnet is the public subnet, which allows instances to be launched with a public IP address.

### Internet Gateway
An internet gateway is a horizontally scaled, redundant, and highly available AWS service that allows communication between instances in the VPC and the internet. In this project, an internet gateway is created and attached to the VPC to allow outbound traffic from the VPC. This enables instances launched in the public subnet to communicate with the internet.

### Route Table
A route table contains a set of rules, called routes, that are used to determine where network traffic is directed. In this project, a route table is created with a default route to the internet gateway, which allows inbound traffic to the VPC. The route table is associated with the public subnet to enable internet connectivity for instances launched in the subnet.

### Security Group for Gitlab Instance
A security group acts as a virtual firewall that controls inbound and outbound traffic for instances launched in a VPC. In this project, a security group is created to control the inbound and outbound traffic for the Gitlab instance. Inbound traffic is allowed on ports 22 (SSH), 80 (HTTP), and 443 (HTTPS), and outbound traffic is allowed on all ports and protocols. This security group is associated with the Gitlab instance to control the traffic allowed to and from the instance.

### Security Group for Gitlab Runner
Similar to the security group for the Gitlab instance, a security group is created to control the inbound and outbound traffic for the Gitlab runner instance. Inbound traffic is allowed on ports 22 (SSH), 80 (HTTP), and 443 (HTTPS), and outbound traffic is allowed on all ports and protocols. This security group is associated with the Gitlab runner instance to control the traffic allowed to and from the instance.

### Gitlab Instance
A Gitlab instance is launched using the specified Amazon Machine Image (AMI), instance type, and subnet. The instance is assigned a public IP address to enable internet connectivity, and is associated with the Gitlab security group. A user data script is executed on the instance to install the necessary software and configure the Gitlab instance. This Gitlab instance provides the Gitlab environment where users can create and manage their repositories, pipelines, and other CI/CD functionalities.

### Gitlab Runner Instance
A Gitlab runner instance is launched using the specified AMI, instance type, and subnet. The instance is assigned a public IP address to enable internet connectivity, and is associated with the Gitlab runner security group. A user data script is executed on the instance to install the necessary software and configure the Gitlab runner. This Gitlab runner instance provides the necessary infrastructure for executing Gitlab CI/CD pipelines.

## Infrastructure as Code

![Terraform](media/Brainboard_-_Gitlab_POC.png)

Diagram was generated via ![Brainboard](https://www.brainboard.co/)

### Providers:

- `aws`: This provider provisions resources in AWS.

### Resources:

- `aws_vpc`: This resource creates a VPC with a specified CIDR block.
- `aws_subnet`: This resource creates a subnet within the VPC.
- `aws_internet_gateway`: This resource creates an internet gateway and attaches it to the VPC.
- `aws_route_table`: This resource creates a route table with a default route to the internet gateway and associates it with the public subnet.
- `aws_route_table_association`: This resource associates the subnet with the route table.
- `aws_security_group`: This resource creates a security group for the Gitlab instance and a security group for the Gitlab runner instance.
- `aws_instance`: This resource launches the Gitlab instance and the Gitlab runner instance in the specified subnet.
- `null_resource`: This resource is used to execute user data scripts on the Gitlab instance and the Gitlab runner instance.

### User Data Scripts:

- `user_data` (Gitlab instance): This script installs the necessary software and configures the Gitlab instance.
- `user_data` (Gitlab runner instance): This script installs Docker and Gitlab Runner.

### Terraform Flags:

- `terraform init`: Initializes the Terraform directory.
- `terraform plan`: Generates an execution plan.
- `terraform apply`: Applies the changes to the infrastructure.
- `terraform destroy`: Destroys the infrastructure.

### Terraform Input Variables:

- `vpc_cidr`: The CIDR block for the VPC.
- `subnet_cidr`: The CIDR block for the subnet.
- `ami_id`: The ID of the Amazon Machine Image (AMI) to use for the instances.
- `instance_type`: The instance type for the instances.
- `key_name`: The name of the key pair to use for SSH access to the instances.
- `gitlab_hostname`: The hostname for the Gitlab instance.
- `gitlab_registration_token`: The registration token for the Gitlab runner instance.
- `instance_username`: The username to use for SSH access to the instances.
- `aws_ssh_private_key`: The path to the private key file to use for SSH access to the instances.
- `route53_zone_id`: The ID of the Route53 zone for the DNS record.

## Runners

GitLab Runner is an open source continuous integration/continuous delivery (CI/CD) tool that executes jobs in response to triggers from GitLab. It is used to automate the building, testing, and deployment of software projects. GitLab Runner can run on a variety of platforms, including Linux, macOS, and Windows, and can execute jobs using a variety of technologies, including Docker, Kubernetes, and Shell scripts.

### Code Block to Deploy GitLab Runner

The code block to deploy GitLab Runner in the Terraform code for the GitLab setup in AWS consists of the following resources:

- `aws_instance`: This resource launches an EC2 instance to host the GitLab Runner. It uses the specified AMI, instance type, and subnet, and is associated with the GitLab Runner security group.
- `null_resource`: This resource is used to execute user data scripts on the GitLab Runner instance.
- `aws_security_group`: This resource creates a security group for the GitLab Runner instance.

The GitLab Runner instance is associated with the GitLab Runner security group, which allows inbound traffic on ports 22 (SSH), 80 (HTTP), and 443 (HTTPS), and outbound traffic on all ports and protocols.

The user data script for the GitLab Runner instance installs Docker and GitLab Runner, and registers the runner with the GitLab instance using the specified registration token and hostname.

## Script to Register GitLab Runner

This Bash script is used to register a GitLab Runner with a GitLab instance. 

```
#!/bin/bash
sudo gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url http://$2 \
  --registration-token $1 \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

### Usage

Before running the script, make sure that you have the following information:

- `registration_token`: The registration token for the GitLab Runner, which can be obtained from the GitLab instance.
- `gitlab_url`: The URL of the GitLab instance. 

To run the script, execute the following command:

```
sudo bash register_runner.sh <registration_token> <gitlab_url>
```

### Options

The following options are available:

- `--executor`: The executor for the GitLab Runner. In this script, the executor is set to "docker". We use docker to have a clean working environment everytime a pipeline is executed.
- `--docker-image`: The Docker image to use for the GitLab Runner. In this script, the image is set to "alpine:latest".
- `--description`: A description for the GitLab Runner. In this script, the description is set to "docker-runner".
- `--maintenance-note`: Free-form maintainer notes about the GitLab Runner.
- `--tag-list`: A list of tags for the GitLab Runner. In this script, the tags are set to "docker,aws".
- `--run-untagged`: Whether to run untagged jobs on the GitLab Runner. In this script, the value is set to "true".
- `--locked`: Whether the GitLab Runner should be locked for new jobs. In this script, the value is set to "false".
- `--access-level`: The access level for the GitLab Runner. In this script, the value is set to "not_protected".


## Storing Secrets, Variables, and Credentials in GitLab Variables

GitLab variables can be used to store sensitive information such as access keys, tokens, or passwords that are required by your infrastructure code or CI/CD pipeline. GitLab provides several types of variables:

- **Instance variables**: These variables are available to all projects and pipelines within a GitLab instance. They are created and managed by GitLab administrators.

- **Group variables**: These variables are available to all projects and pipelines within a group. They are created and managed by group owners.

- **Project variables**: These variables are available only to the project they are defined in. They are created and managed by project maintainers.

GitLab variables can be defined in the GitLab web interface or via a `.gitlab-ci.yml` file. They can be used to store secrets, such as API keys or access tokens, that are required by your pipeline or infrastructure code.

### Defining Variables in the GitLab Web Interface

To define a variable in the GitLab web interface:

1. Navigate to the project or group where you want to define the variable.
2. Click on **Settings** and then **CI/CD**.
3. Scroll down to the **Variables** section.
4. Click on **Add Variable**.
5. Enter the variable name and value.
6. Choose the variable type (e.g., instance, group, or project).
7. Click **Add Variable** to save the variable.

![Variable](media/Screen_Shot_2023-05-07_at_4.37.11_PM.png)

### Defining Variables in the .gitlab-ci.yml file

To define a variable in the `.gitlab-ci.yml` file:

1. Open the file in a text editor.
2. Add the `variables` section to the job that requires the variable.
3. Define the variable as a key-value pair.

For example:

```
variables:
  AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
  AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}
```

In this example, the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables are defined as key-value pairs. The values for these variables are passed to the job as environment variables when the pipeline is run.

### Using Variables in Terraform Code

To use GitLab variables in Terraform code, you can define them as input variables in your Terraform modules. For example:

```
variable "aws_access_key" {
  description = "AWS access key"
}

variable "aws_secret_key" {
  description = "AWS secret key"
}
```

In this example, the `aws_access_key` and `aws_secret_key` variables are defined as input variables. When running the Terraform code, these variables are passed to Terraform using the `-var` flag:

```
terraform apply -var "aws_access_key=${AWS_ACCESS_KEY_ID}" -var "aws_secret_key=${AWS_SECRET_ACCESS_KEY}"
```

### Using Variables in CI/CD Pipeline

To use GitLab variables in a CI/CD pipeline, you can reference them using the `${VARIABLE_NAME}` syntax in the `.gitlab-ci.yml` file. For example:

```
variables:
  AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
  AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}

deploy:
  script:
    - terraform apply -var "aws_access_key=${AWS_ACCESS_KEY_ID}" -var "aws_secret_key=${AWS_SECRET_ACCESS_KEY}"
```

In this example, the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables are defined as GitLab variables and then passed to the `terraform apply` command as input variables using the `-var` flag.

A similar approach is used in the Register_Runner Stage. 




